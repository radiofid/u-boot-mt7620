/*
 * (C) Copyright 2003
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include "../../autoconf.h"
#include <common.h>
#include <command.h>
#include <asm/addrspace.h>
#include <rt_mmap.h>
#include <asm/io.h>

//#include "LzmaDecode.h"

//#define MAX_SDRAM_SIZE	(64*1024*1024)
//#define MIN_SDRAM_SIZE	(8*1024*1024)
#define MAX_SDRAM_SIZE	(256*1024*1024)
#define MIN_SDRAM_SIZE	(8*1024*1024)

#ifdef SDRAM_CFG_USE_16BIT
#define MIN_RT2880_SDRAM_SIZE	(16*1024*1024)
#else
#define MIN_RT2880_SDRAM_SIZE	(32*1024*1024)
#endif

DECLARE_GLOBAL_DATA_PTR;
#define DEBUG

/*
 * Check memory range for valid RAM. A simple memory test determines
 * the actually available RAM size between addresses `base' and
 * `base + maxsize'.
 */
long get_ram_size(volatile long *base, long maxsize)
{
	volatile long *addr;
	long           save[32];
	long           cnt;
	long           val;
	long           size;
	int            i = 0;

	for (cnt = (maxsize / sizeof (long)) >> 1; cnt > 0; cnt >>= 1) {
		addr = base + cnt;	/* pointer arith! */
		save[i++] = *addr;
		
		*addr = ~cnt;

		
	}

	addr = base;
	save[i] = *addr;

	*addr = 0;

	
	if ((val = *addr) != 0) {
		/* Restore the original data before leaving the function.
		 */
		*addr = save[i];
		for (cnt = 1; cnt < maxsize / sizeof(long); cnt <<= 1) {
			addr  = base + cnt;
			*addr = save[--i];
		}
		return (0);
	}

	for (cnt = 1; cnt < maxsize / sizeof (long); cnt <<= 1) {
		addr = base + cnt;	/* pointer arith! */

	//	printf("\n retrieve addr=%08X \n",addr);
			val = *addr;
		*addr = save[--i];
		if (val != ~cnt) {
			size = cnt * sizeof (long);
			
		//	printf("\n The Addr[%08X],do back ring  \n",addr);
			
			/* Restore the original data before leaving the function.
			 */
			for (cnt <<= 1; cnt < maxsize / sizeof (long); cnt <<= 1) {
				addr  = base + cnt;
				*addr = save[--i];
			}
			return (size);
		}
	}

	return (maxsize);
}



long int initdram(int board_type)
{
	ulong size, max_size       = MAX_SDRAM_SIZE;
	ulong our_address;
  
	asm volatile ("move %0, $25" : "=r" (our_address) :);

	/* Can't probe for RAM size unless we are running from Flash.
	 */
#if 0	 
	#if defined(CFG_RUN_CODE_IN_RAM)

	printf("\n In RAM run \n"); 
    return MIN_SDRAM_SIZE;
	#else

	printf("\n In FLASH run \n"); 
    return MIN_RT2880_SDRAM_SIZE;
	#endif
#endif 
    
#if defined (RT2880_FPGA_BOARD) || defined (RT2880_ASIC_BOARD)
	if (PHYSADDR(our_address) < PHYSADDR(PHYS_FLASH_1))
	{
	    
		//return MIN_SDRAM_SIZE;
		//fixed to 32MB
		printf("\n In RAM run \n");
		return MIN_SDRAM_SIZE;
	}
#endif
	 


	size = get_ram_size((ulong *)CFG_SDRAM_BASE, MAX_SDRAM_SIZE);
	if (size > max_size)
	{
		max_size = size;
	//	printf("\n Return MAX size!! \n");
		return max_size;
	}
//	printf("\n Return Real size =%d !! \n",size);
	return size;
	
}

int checkboard (void)
{
	puts ("Board: Ralink APSoC ");
	return 0;
}

#define REG_PIO         (RALINK_PIO_BASE)
#define REG_PIO_DATA    (REG_PIO + 0x20)
#define REG_PIO2_DATA    (REG_PIO + 0x48)
#define REG_PIO_DIR     (REG_PIO + 0x24)
#define REG_PIO2_DIR     (REG_PIO + 0x4C)
#define REG_GPIOMODE    (PHYSADDR(RALINK_SYSCTL_BASE + 0x60))

int misc_init_r(void){
    // set uart to gpio mode
#if defined(CONFIG_BOARD_MT02)
#define RESET_BUTTON_STATE  (readl(PHYSADDR(REG_PIO_DATA)) & (1 << 17))
#define POWER_LED_SET(x)    (writel(((readl(REG_PIO2_DATA) & ~(1 << 5)) | ((x) << 5)), PHYSADDR(REG_PIO2_DATA)))
    unsigned long reg_gpiomode = readl(REG_GPIOMODE);
    reg_gpiomode &= ~( 0x3 << 21);
    reg_gpiomode |=  ( 0x2 << 21); // set wdt_rst to gpio
    reg_gpiomode |=  ( 0x1 << 9); // set rgmii1 to gpio
    writel( reg_gpiomode, REG_GPIOMODE);

#ifdef DEBUG
        printf("pio dir state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO_DIR),readl(PHYSADDR(REG_PIO_DIR)) );
        printf("pio2 dir state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO2_DIR),readl(PHYSADDR(REG_PIO2_DIR)) );
#endif
    
    // set direction of gpios    
    writel( 
        (readl(PHYSADDR(REG_PIO2_DIR)) | (1 << 5)) // set led_pwr out
        , PHYSADDR(REG_PIO2_DIR)
    );

#ifdef DEBUG
        printf("pio2 dir state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO2_DIR),readl(PHYSADDR(REG_PIO2_DIR)) );
#endif

    writel( 
        (readl(PHYSADDR(REG_PIO_DIR)) & ~(1 << 17)) // set button in
        , PHYSADDR(REG_PIO_DIR)
    );
#ifdef DEBUG
        printf("pio dir state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO_DIR), readl(PHYSADDR(REG_PIO_DIR)));
#endif
#elif defined(CONFIG_BOARD_MT00)
#define RESET_BUTTON_STATE  (!(readl(PHYSADDR(REG_PIO2_DATA)) & (1 << 0)))
#define POWER_LED_SET(x)    (writel(((readl(REG_PIO2_DATA) & ~(1 << 2)) | ((!x) << 2)), PHYSADDR(REG_PIO2_DATA)))

    unsigned long reg_gpiomode = readl(REG_GPIOMODE);
    reg_gpiomode |= ( 0x1 << 9); // set rgmii1 to gpio
    writel( reg_gpiomode, REG_GPIOMODE);
#ifdef DEBUG
        printf("pio dir state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO_DIR),readl(PHYSADDR(REG_PIO_DIR)) );
        printf("pio2 dir state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO2_DIR),readl(PHYSADDR(REG_PIO2_DIR)) );
#endif
    // set direction of gpios
    writel( 
        (readl(PHYSADDR(REG_PIO2_DIR)) & ~(1 << 0)) // set button in
        , PHYSADDR(REG_PIO2_DIR)
    );

    writel( 
        (readl(PHYSADDR(REG_PIO2_DIR)) | (1 << 2)) // set power led out
        , PHYSADDR(REG_PIO2_DIR)
    );
    
    
#ifdef DEBUG
        printf("pio dir state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO_DIR), readl(PHYSADDR(REG_PIO_DIR)));
        printf("pio2 dir state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO2_DIR),readl(PHYSADDR(REG_PIO2_DIR)) );
        printf("pio2 data state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO2_DATA),readl(PHYSADDR(REG_PIO2_DATA)) );
#endif
#elif defined(CONFIG_BOARD_S_TERRA)
#define POWER_LED_SET(x)    (writel(((readl(REG_PIO2_DATA) & ~(1 << 0)) | ((!x) << 0)), PHYSADDR(REG_PIO2_DATA)))
#define RESET_BUTTON_STATE  ((readl(PHYSADDR(REG_PIO2_DATA)) & (1 << 9)))
#ifdef DEBUG
        printf("pio dir state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO_DIR),readl(PHYSADDR(REG_PIO_DIR)) );
        printf("pio2 dir state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO2_DIR),readl(PHYSADDR(REG_PIO2_DIR)) );
#endif
    unsigned long reg_gpiomode = readl(REG_GPIOMODE);
    reg_gpiomode &= ~( 0x3 << 21);
    reg_gpiomode |=  ( 0x1 << 9); // set rgmii1 to gpio
    writel( reg_gpiomode, REG_GPIOMODE);
    writel(
        (readl(PHYSADDR(REG_PIO2_DIR)) & ~(1 << 9)) // set button in
        , PHYSADDR(REG_PIO2_DIR)
    );

    writel(
        (readl(PHYSADDR(REG_PIO2_DIR)) | (1 << 0)) // set power led out
        , PHYSADDR(REG_PIO2_DIR)
    );
    
#ifdef DEBUG
        printf("pio dir state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO_DIR), readl(PHYSADDR(REG_PIO_DIR)));
        printf("pio2 dir state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO2_DIR),readl(PHYSADDR(REG_PIO2_DIR)) );
        printf("pio2 data state:(addr:0x%X) 0x%X\n",PHYSADDR(REG_PIO2_DATA),readl(PHYSADDR(REG_PIO2_DATA)) );
#endif
#else
#error "Board not selected"
#endif

    POWER_LED_SET(0);

    int i = 0;
#if defined(CONFIG_BOARD_S_TERRA)
    if (!RESET_BUTTON_STATE) {
	setenv("bootargs", "console=ttyS0,57600 cs_rst=1");
    } else {
	setenv("bootargs", "console=ttyS0,57600 cs_rst=0");
    }
#else
    if (!RESET_BUTTON_STATE) {
	setenv("bootargs", "console=ttyS0,57600");
    }
    
#endif

    while (!RESET_BUTTON_STATE) {
        if (i >= 0 && i < 10 ) {
            POWER_LED_SET((i % 2));
        } else if (i >= 10) {
            POWER_LED_SET(1);
        } else if( i >= 20 ) {
           POWER_LED_SET(0);
        }

        udelay(500000);
        i++;
    }
       if ( getenv("serverip") == NULL ) { setenv("serverip", "192.168.1.10"); }
       if ( getenv("ipaddr") == NULL ) { setenv("ipaddr", "192.168.1.1"); }
       puts("Bootmode: ");
       gd->boot_mode = 2;
       if (i >= 20) {
               puts("usedefaults\n");
               setenv("bootmode", "usedefaults");
               gd->boot_mode = 0;
       } else if (i >= 10) {
               puts("rescue\n");
               setenv("bootmode", "rescue");
               gd->boot_mode = 1;
       } else {
               puts("flash\n");
               setenv("bootmode", "flash");
               gd->boot_mode = 2;
       }
       return 0;
}